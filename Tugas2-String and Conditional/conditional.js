//Tugas Conditional if-else

var nama = 'Shelby'
var peran = 'Werewolf'
var welcomeMsg = "Selamat datang di Dunia Werewolf, " +nama

if (nama == ' ' && peran == ' '){
    console.log("Nama harus diisi!")
}
else if (nama == 'Shelby' && peran == ' '){
    console.log("Halo " +nama, "," , "Pilih peranmu untuk memulai game!")
}
else if(peran == 'Villager'){
    console.log(welcomeMsg)
    console.log("Halo " +peran, "" +nama, "Hati-hati terhadap Werewolf!")
}
else if(peran == 'Seer'){
    console.log(welcomeMsg)
    console.log("Halo " +peran, "" +nama, "Gunakan kekuatanmu untuk mencari tahu Werewolf diantaramu!")
}
else if(peran == 'Werewolf'){
    console.log(welcomeMsg)
    console.log("Halo " +peran, "" +nama, "Mangsa semua penduduk desa ini!")
}
//Pembatas
console.log("------------------------------------------------------")

//Tugas Conditional Switch case
var tanggal = 28
var bulan = 12
var tahun = 2017

if(tanggal>=1 && tanggal<=31 && bulan>=1 && bulan<=12 && tahun>=1900 && tahun<=2200){
switch(bulan){
    case 1: {console.log(""+tanggal, "Januari", ""+tahun); break; }
    case 2: {console.log(""+tanggal, "Februari",""+tahun); break; }
    case 3: {console.log(""+tanggal, "Maret",""+tahun); break; }
    case 4: {console.log(""+tanggal, "April", ""+tahun); break; }
    case 5: {console.log(""+tanggal, "Mei", ""+tahun); break; }
    case 6: {console.log(""+tanggal, "Juni", ""+tahun); break; }
    case 7: {console.log(""+tanggal, "Juli", ""+tahun); break; }
    case 8: {console.log(""+tanggal, "Agustus", ""+tahun); break; }
    case 9: {console.log(""+tanggal, "September", ""+tahun); break; }
    case 10: {console.log(""+tanggal, "Oktober", ""+tahun); break; }
    case 11: {console.log(""+tanggal, "November", ""+tahun); break; }
    case 12: {console.log(""+tanggal, "Desember", ""+tahun); break; }

}
}
else console.log("Tanggal tidak valid")



