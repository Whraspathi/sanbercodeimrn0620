import React from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class SkillScreen extends React.Component {
    render() {
      return (
        <View style={styles.container}>

          <View style={styles.navBar}>
            <Text style={styles.navText}>My Profile</Text>
            <Text style={styles.navText}>Current Skills</Text>
            <Text style={styles.navText}>Logout</Text>
          </View>

          <View style={styles.userBox}>
            <Text style={styles.userText}>Welcome, Confucius!</Text>
          </View>

          <View style={styles.languageBox}>
            <Text style={styles.languageText}>Programming Language</Text>
          </View>

          <View style={styles.circleBox}>  
            <View style={styles.circle1}>
              <Image source={require('./images/java.jpg')} style={{height:50, width:50, alignSelf:'center', borderRadius:112.5, marginTop:23}} />
              {/* <Text style={{alignSelf:'center', marginTop:33, fontSize:18, color:'white'}}>85%</Text> */}
            </View>
            <View style={styles.circle2}>
              <Image source={require('./images/python.png')} style={{height:50, width:50, alignSelf:'center', borderRadius:112.5, marginTop:23}} />
              {/* <Text style={{alignSelf:'center', marginTop:33, fontSize:18, color:'white'}}>75%</Text> */}
            </View>
          </View>

          <View style={styles.textbox}>
            <Text style={styles.textLang}>Intermediate Java</Text>
            <Text style={styles.textLang}>Intermediate Python</Text>
          </View>

          <View style={styles.textbox}>
            <Text style={styles.textLang}>85%</Text>
            <Text style={styles.textLang}>75%</Text>
          </View>

          <View style={styles.line}/>

          <View style={styles.languageBox}>
            <Text style={styles.languageText}>Frameworks</Text>
          </View>

          <View style={styles.circleBox2}>
            <View style={styles.circle1}>
              <Image source={require('./images/bootstrap.png')} style={{height:50, width:50, alignSelf:'center', borderRadius:112.5, marginTop:23}} />
              {/* <Text style={{alignSelf:'center', marginTop:33, fontSize:18, color:'white'}}>60%</Text> */}
            </View>
          </View>

          <View style={styles.textbox}>
            <Text style={styles.textLang}>Advanced Bootstrap</Text>
          </View>

          <View style={styles.textbox}>
            <Text style={styles.textLang}>60%</Text>
          </View>

        </View>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#28313B'

  },
  navBar:{
    flexDirection:'row',
    justifyContent:'space-evenly',
    height: 40,
    backgroundColor: '#547B97',
    paddingTop:15
  },
  navText:{
    fontSize:18,
    color:'#FFF8F8'

  },
  userBox:{
    paddingTop:30,
    height:170,
    width:200,
  },
  userText:{
    fontSize:36,
    marginLeft:12,
    color:'white',
    textAlign:'justify'
  },
  languageText:{
    flexDirection:'row',
    color: 'white',
    fontSize:18,
    justifyContent:'flex-start',
    marginLeft:12
  },
  languageBox:{
    marginBottom:5,
    flexDirection:'row',
    height:30
  },
  circleBox:{
    flexDirection:'row',
    justifyContent:'space-around'
  },
  circleBox2:{
    flexDirection:'row',
    justifyContent:'space-around'
  },
  circle1:{
    width: 145,
    height: 145,
    borderRadius: 145 / 2,
    borderColor: '#C4C4C4',
    borderWidth: 25,
    alignSelf:'flex-start'
  },
  circle2:{
    width: 145,
    height: 145,
    borderRadius: 145 / 2,
    borderColor: '#C4C4C4',
    borderWidth: 25,
    alignContent:'flex-end'
  },
  textbox:{
    height:40,
    flexDirection:'row',
    justifyContent:'space-around'

  },
  textLang:{
    marginTop:10,
    fontSize:18,
    color:'white'
  },
  line:{
    marginTop:5,
    marginBottom:10,
    height:2,
    backgroundColor:'#C6D3E8'
  }
  

  
});