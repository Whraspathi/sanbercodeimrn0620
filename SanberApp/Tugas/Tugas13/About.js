import React, { Component } from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
          <View style={styles.navBar}>
            <Icon name='arrow-back' size={40} color={'black'}/>
          </View>
          <View style={styles.profilePic}>
            <Image source={require('./images/confucius.jpg')} style={{width:80,height:80,borderRadius:40,alignSelf:'center'}} />
            <Text style={styles.username}>Confucius</Text>
          </View>
          <View style={styles.info}>
            <Text style={styles.socmedText}>Social Media</Text>
            <View style={styles.socmedBox}>
              <Image source={(require('./images/twitter.png'))} style={styles.icons}/>
              <Text style={styles.socmedUser}>@Confucius</Text>
            </View>
            <View style={styles.socmedBox}>
              <Image source={(require('./images/instagram.png'))} style={styles.icons}/>
              <Text style={styles.socmedUser}>@Confucius</Text>
            </View>
            <View style={styles.socmedBox}>
              <Image source={(require('./images/facebook.png'))} style={styles.icons}/>
              <Text style={styles.socmedUser}>Real Confucius</Text>
            </View>
            <Text style={styles.socmedText}>Portofolio</Text>
            <View style={styles.socmedBox}>
              <Image source={(require('./images/github.jpg'))} style={styles.icons}/>
              <Text style={styles.socmedUser}>@Confucius</Text>
            </View>
            <View style={styles.socmedBox}>
              <Image source={(require('./images/gitlab.png'))} style={styles.icons}/>
              <Text style={styles.socmedUser}>@Confucius</Text>
            </View>
          </View>
        </View>

      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
      height: 60,
      backgroundColor: 'white',
      paddingHorizontal:5,
      paddingVertical:10,
      color:'#000000'
    },
  profilePic:{
    height:225,
    justifyContent:'center'
  },
  username:{
    fontSize:24,
    paddingTop:20,
    alignSelf:'center'
  },
  info:{
    flex:1,
    flexBasis: 'auto',
    backgroundColor:'#28313B',
    alignSelf:'center',
    paddingVertical:10,
    width:400,
    borderRadius:20
  },
  socmedText:{
    textAlignVertical:'top',
    alignSelf:'center',
    color:'white',
    fontSize:22,
    paddingBottom:40
  },
  socmedBox:{
    flexDirection:'row',
  }, 
  icons:{
    width:40,
    height:40,
    marginBottom:20,
    alignSelf:"flex-start",
    marginLeft:16
  },
  socmedUser:{
    fontSize: 18,
    color:'white',
    marginLeft:40,
    textAlign:'center'

  }
  
});