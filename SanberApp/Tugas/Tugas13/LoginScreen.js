import React, { Component } from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
        <View style={styles.logo}>
          <Text style={styles.title}>Appostrophe</Text>
            <Image source={require('./images/AppLogo.png')} style={{width:80,height:80}} />
            <View style={styles.isi}>
                <View style={styles.isi2}>
                    <Text style={styles.isiText}>Username</Text>
                    <View style={styles.box}></View>
                </View>
                <View style={styles.isi2}>
                    <Text style={styles.isiText}>Password</Text>
                    <View style={styles.box}></View>
                </View>
            </View>
            <View style={styles.login}>
                <Text style={styles.loginText}>Login</Text>
            </View>
        </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent:'space-around',
    backgroundColor:'#28313B'
  },
  title:{
    fontSize:22,
    color: 'white',
    paddingBottom:40
  },
  logo:{
      alignItems:'center',
      paddingBottom:40
  },
  isi:{
      paddingTop:50,
      paddingBottom:50,
  },
  isi2:{
      paddingTop: 20,

  },
  isiText:{
      fontSize: 22,
      color : 'white',
      alignSelf:'flex-start'  
  },
  box:{
      height:35,
      backgroundColor:'#9D9D9D',
      alignSelf:'center',
      width:250,
      borderRadius: 7
  },
  login:{
      backgroundColor: '#18A3AC',
      height:35,
      width:250,
      justifyContent: 'center',
      borderRadius: 7
  },
  loginText:{
      fontSize: 11,
      color: 'white',
      textAlign: 'center'
  }
});