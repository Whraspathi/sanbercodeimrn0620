var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var waktu = (time,i) =>{
    readBooksPromise(time,books[i])
    .then(function(sisaWaktu){
        return readBooksPromise(time,books[i+1])
    })
    .then(function(sisaWaktu){
        return readBooksPromise(time,books[i+2])
    })
    .catch(function(sisaWaktu){
        return sisaWaktu
    }) 
}
waktu(10000,0)