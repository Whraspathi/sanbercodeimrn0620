//Soal 1
function range (startNum, finishNum){
    if(!startNum || !finishNum){
    return -1
    }

    else if(startNum!=="" && finishNum!==""){
        var angka = []
        if(startNum<finishNum){
            for(let i=startNum;i<=finishNum;i++){
                angka.push(i)
            }   
        }
        else if(startNum>finishNum){
            for(let i=finishNum;i<=startNum;i++){
                angka.unshift(i)
            }
        }
    return angka
    }
    
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

//Pembatas
console.log("--------------------------------------------------")

//Soal 2
function rangeWithStep(firstNum, lastNum, step){
    var angka = []
        if(firstNum<lastNum){
            for(let i=firstNum;i<=lastNum;i+=step){
                angka.push(i)
            }   
        }
        else if(firstNum>lastNum){
            for(let i=firstNum;i>=lastNum;i-=step){
                angka.push(i)
            }
        }
    return angka
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
//Pembatas
console.log("--------------------------------------------------")

//Soal 3
function sum(awal, terakhir, step1){
    var angka2 = []
    if(!step1)
    step1 = 1
    if(awal && !terakhir)
    return awal
    else{
        if(awal<terakhir){
            for(let i=awal;i<=terakhir;i+=step1){
                angka2.push(i)
            }   
        }
        else if(awal>terakhir){
            for(let i=awal;i>=terakhir;i-=step1){
                angka2.push(i)
            }
        }
        total = 0
        for(let i in angka2){
            total += angka2[i]
        }
    }    
    return total
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
//Pembatas
console.log("--------------------------------------------------")

//Soal 4
function dataHandling(i){
    for(let i=0;i<4;i++){
            console.log("Nomor ID: ", input[i][0])
            console.log("Nama Lengkap: ", input[i][1])
            console.log("TTL: ", input[i][2], " " + input[i][3])
            console.log("Hobi: ",input[i][4])
            console.log("\n")
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling()
//Pembatas
console.log("--------------------------------------------------")

//Soal 5
function balikKata(string){
    var kata = ""
    last = string.length-1
    while(last>=0){
        kata+= string[last]
        last--
    }
    return kata
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
//Pembatas
console.log("--------------------------------------------------")

//Soal 6
function dataHandling2(input2){
    input2.splice(1,2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    input2.splice(4,2, "Pria", "SMA Internasional Metro")
    console.log(input2)

    var bulan = input2[3].split("/")
    switch(bulan[1]){
        case "01": {console.log("Januari"); break;}
        case "02": {console.log("Februari"); break;}
        case "03": {console.log("Maret"); break;}
        case "04": {console.log("April"); break;}
        case "05": {console.log("Mei"); break;}
        case "06": {console.log("Juni"); break;}
        case "07": {console.log("Juli"); break;}
        case "08": {console.log("Agustus"); break;}
        case "09": {console.log("September"); break;}
        case "10": {console.log("Oktober"); break;}
        case "11": {console.log("November"); break;}
        case "12": {console.log("Desember"); break;}
        default: {console.log("input tidak valid"); break;}

    }
    var tanggal = bulan.join("-")
    bulan.sort(function (a,b){return b-a})
    console.log(bulan)
    console.log(tanggal)
    

    var nama = input2[1]
    var nama1 = nama.slice(0,14)
    console.log(nama1)
}
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
dataHandling2(input2)